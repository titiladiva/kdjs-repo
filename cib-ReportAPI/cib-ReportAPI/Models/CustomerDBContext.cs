﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cibReportAPI.Models
{
    public class CustomerDBContext: DbContext
    {
        public CustomerDBContext(DbContextOptions<CustomerDBContext>options) : base(options)
        {

        }
        public DbSet<CustomerDB>Customers { get; set; }
    }
}
