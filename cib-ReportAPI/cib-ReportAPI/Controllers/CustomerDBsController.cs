﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using cibReportAPI.Models;

namespace cibReportAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerDBsController : ControllerBase
    {
        private readonly CustomerDBContext _context;

        public CustomerDBsController(CustomerDBContext context)
        {
            _context = context;
        }

        // GET: api/CustomerDBs
        [HttpGet]
        public IEnumerable<CustomerDB> GetCustomers()
        {
            return _context.Customers;
        }

        // GET: api/CustomerDBs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerDB([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customerDB = await _context.Customers.FindAsync(id);

            if (customerDB == null)
            {
                return NotFound();
            }

            return Ok(customerDB);
        }

        // PUT: api/CustomerDBs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomerDB([FromRoute] int id, [FromBody] CustomerDB customerDB)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customerDB.CustomerId)
            {
                return BadRequest();
            }

            _context.Entry(customerDB).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerDBExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CustomerDBs
        [HttpPost]
        public async Task<IActionResult> PostCustomerDB([FromBody] CustomerDB customerDB)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Customers.Add(customerDB);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCustomerDB", new { id = customerDB.CustomerId }, customerDB);
        }

        // DELETE: api/CustomerDBs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerDB([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customerDB = await _context.Customers.FindAsync(id);
            if (customerDB == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(customerDB);
            await _context.SaveChangesAsync();

            return Ok(customerDB);
        }

        private bool CustomerDBExists(int id)
        {
            return _context.Customers.Any(e => e.CustomerId == id);
        }
    }
}